
let btnMain = document.querySelector('.btn-main');
btnMain.addEventListener('click', addInput);
let formNum;

function addInput() {
   btnMain.insertAdjacentHTML('afterend',
      `<form class="number-form" action="#" type="POST">
         <label class="input-number">
            Введіть діаметр кола
            <input id="diameter" type="number" />
         </label>
         <button type="submit" class="btn-circle">"Намалювати"</button>
      </form>`);
   btnMain.remove();
   formNum = document.querySelector('.number-form');
   formNum.addEventListener('submit', addCircles);
}


function addCircles() {
   formNum.insertAdjacentHTML('afterend',
      `<div id="main-div-circle" 
      style="margin-top: 40px; white-space: nowrap;"
      ></div>`)

   let mainDiv = document.getElementById('main-div-circle');
   mainDiv.addEventListener('click', removeCircle);
   for (let i = 1; i < 11; i++) {
      mainDiv.insertAdjacentHTML('beforeend', `<div id='row ${i}' class='div-row' style="text-align: left;"></div>`)
   }

   let diameter;
   //!Обмежив 500, щоб випадково не ввести завелике число
   if (document.getElementById('diameter').value < 500) { diameter = document.getElementById('diameter').value }

   Array.from(mainDiv.children).forEach(elem => {
      for (let j = 1; j < 11; j++) {
         elem.insertAdjacentHTML('beforeend', `<div class='div-item'
         style="display: inline-block; height: ${diameter}px; width: ${diameter}px; 
         background-color: ${getRandomColor()}; border-radius: 50%; margin-left: 3px;
         text-align: center; align-items: center; "
         >${j}</div>`);
      }
   });
}


function removeCircle(event) {
   if (event.target.closest('.div-item')) {
      event.target.remove()
   }
}


function getRandomColor() {
   let letters = '0123456789ABCDEF';
   let color = '#';
   for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
   }
   return color;
}




//*пробував таблицею
// function addCircles() {
//    let formNum = document.querySelector('.number-form');
//    formNum.insertAdjacentHTML('afterend',
//       `<table style="margin-top: 40px; margin-left: auto; margin-right: auto;">
//          <tbody id="tbody-circle">
//          </tbody>
//       </table>`)

//    let tb = document.getElementById('tbody-circle');
//    for (let i = 0; i < 10; i++) {
//       let tr = document.createElement('TR');
//       tr.innerHTML = `<tr></tr>`;
//       tb.appendChild(tr);
//    }

//    Array.from(document.getElementsByTagName("tr")).forEach(elem => {
//       for (let j = 0; j < 10; j++) {
//          let td = document.createElement('TD');
//          td.innerHTML = `<td style="margin-left: 5px;"><div
//          style="height: 20px; width: 20px; background-color: blue; border-radius: 50%;"
//          ></div></td>`;
//          elem.appendChild(td);
//       }
//    });
// }